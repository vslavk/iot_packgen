#ifndef HDR_RANDNUM_GEN_RANDNUM_NORM_H_
#define HDR_RANDNUM_GEN_RANDNUM_NORM_H_

#include "../libs.h"

int gen_norm_randnum (double* _ret_num, double _mu_par, double _sig_par);


#endif /* HDR_RANDNUM_GEN_RANDNUM_NORM_H_ */
