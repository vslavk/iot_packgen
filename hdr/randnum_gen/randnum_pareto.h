#ifndef HDR_RANDNUM_GEN_RANDNUM_PARETO_H_
#define HDR_RANDNUM_GEN_RANDNUM_PARETO_H_

#include "../libs.h"

int gen_pareto_randnum (double* _ret_num, double _a_par, double _num_offset);


#endif /* HDR_RANDNUM_GEN_RANDNUM_PARETO_H_ */
