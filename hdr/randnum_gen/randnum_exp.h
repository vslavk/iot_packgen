#ifndef HDR_RANDNUM_GEN_RANDNUM_EXP_H_
#define HDR_RANDNUM_GEN_RANDNUM_EXP_H_

#include "../libs.h"

int gen_exp_randnum (double* _ret_num, double _l_par);

#endif /* HDR_RANDNUM_GEN_RANDNUM_EXP_H_ */
