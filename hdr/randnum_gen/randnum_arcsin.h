#ifndef HDR_RANDNUM_GEN_RANDNUM_ARCSIN_H_
#define HDR_RANDNUM_GEN_RANDNUM_ARCSIN_H_

#include "../libs.h"

int gen_arcsin_randnum (double* _ret_num, double _a_par, double _b_par);

#endif /* HDR_RANDNUM_GEN_RANDNUM_ARCSIN_H_ */
