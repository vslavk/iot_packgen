#ifndef HDR_RANDNUM_GEN_RANDNUM_UNIFORM_H_
#define HDR_RANDNUM_GEN_RANDNUM_UNIFORM_H_

#include "../libs.h"

int get_random_str (double** _ret_arr, int _str_size);
int gen_fib_randnum (double* _ret_num, double** _fib_arr, int _arr_size, int _a_par, int _b_par);
int gen_uniform_randnum (double* _ret_num, double _a_par, double _b_par);

#endif
