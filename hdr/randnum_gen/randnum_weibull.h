#ifndef HDR_RANDNUM_GEN_RANDNUM_WEIBULL_H_
#define HDR_RANDNUM_GEN_RANDNUM_WEIBULL_H_

#include "../libs.h"

int gen_weibull_randnum (double* _ret_num, double _a_par, double _b_par, double _num_offset);

#endif
