/*
 * randnum.h
 *
 *  Created on: 3 апр. 2020 г.
 *      Author: vslavk
 */

#ifndef HDR_RANDNUM_GEN_RANDNUM_H_
#define HDR_RANDNUM_GEN_RANDNUM_H_

#include "randnum_uniform.h"
#include "randnum_norm.h"
#include "randnum_exp.h"
#include "randnum_weibull.h"
#include "randnum_pareto.h"
#include "randnum_arcsin.h"
#include "randnum_simson.h"
#include "randnum_chi2.h"
#include "randnum_erlang.h"
#include "randnum_gamma.h"

typedef enum
{
	RNGEN_UNIFORM = 0x20,
	RNGEN_EXP = 0x21,
	RNGEN_NORM = 0x22,
	RNGEN_WEIBULL = 0x23,
	RNGEN_PARETO = 0x24,
	RNGEN_CHI2 = 0x25,
	RNGEN_ARCSIN = 0x26,
	RNGEN_SIMSON = 0x27,
	RNGEN_GAMMA = 0x28,
	RNGEN_ERLANG = 0x29,
	RNGEN_CONST = 0x30
} randnum_gen_types;

#endif /* HDR_RANDNUM_GEN_RANDNUM_H_ */
