#ifndef HDR_RANDNUM_GEN_RANDNUM_SIMSON_H_
#define HDR_RANDNUM_GEN_RANDNUM_SIMSON_H_

#include "../libs.h"

int gen_simson_randnum (double* _ret_num, double _a_par, double _b_par);

#endif /* HDR_RANDNUM_GEN_RANDNUM_SIMSON_H_ */
