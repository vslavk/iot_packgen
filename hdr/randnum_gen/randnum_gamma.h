#ifndef HDR_RANDNUM_GEN_RANDNUM_GAMMA_H_
#define HDR_RANDNUM_GEN_RANDNUM_GAMMA_H_

#include "../libs.h"
#include "randnum_exp.h"
#include "randnum_erlang.h"

int gen_gamma_method_less_then_one (double* _ret_num, double _alpha, double _lambda);
int gen_gamma_randnum (double* _ret_num, double _alpha, double _lambda);

#endif /* HDR_RANDNUM_GEN_RANDNUM_GAMMA_H_ */
