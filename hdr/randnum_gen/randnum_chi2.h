#ifndef HDR_RANDNUM_GEN_RANDNUM_CHI2_H_
#define HDR_RANDNUM_GEN_RANDNUM_CHI2_H_

#include "../libs.h"
#include "randnum_norm.h"

int gen_chi2_randnum (double* _ret_num, double _n_par);

#endif /* HDR_RANDNUM_GEN_RANDNUM_CHI2_H_ */
