#ifndef HDR_RANDNUM_GEN_RANDNUM_ERLANG_H_
#define HDR_RANDNUM_GEN_RANDNUM_ERLANG_H_

#include "../libs.h"

int gen_erlang_randnum (double *_ret_num, double _alpha, double _lambda);

#endif /* HDR_RANDNUM_GEN_RANDNUM_ERLANG_H_ */
