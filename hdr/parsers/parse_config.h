#ifndef HDR_PARSERS_PARSE_CONFIG_H_
#define HDR_PARSERS_PARSE_CONFIG_H_

#include "../libs.h"
#include "parse_cmd.h"
#include "../randnum_gen/randnum.h"

typedef enum
{
	GENPROT_UDP = 0x50,
	GENPROT_TCP = 0x51,
	GENPROT_HTTPv1 = 0x52,
	GENPROT_HTTPv2 = 0x53,
	GENPROT_WEBSOCKET = 0x54,
	GENPROT_COAP = 0x55,
	GENPROT_MQTT = 0x56,
	GENPROT_STOMP = 0x57,
	GENPROT_OPCUA = 0x58,
	GENPROT_MODBUS = 0x59
} gen_protocols;

typedef struct
{
	double hit_prob;
	int distr_type;
	double param1;
	double param2;
	double offset;
} distr_config;

typedef struct
{
	int protocol;
	double gen_time; //in seconds
	int attached_streams;

	int time_distrs_quantity;
	distr_config* time_distrs;

	int packsize_distrs_quantity;
	distr_config* packsize_distrs;

	//For mqtt
	char* topic;
	//For http
	char* uri;
	char* method;
	char* data_type;
	//etc....
} stream_config;

typedef struct
{
	char* dst_addr;
	char* dst_mask;
	int dst_port;
	int stream_conf_quantity;
	stream_config* streams_configs;
} work_config;

int read_config_file(char** _ret_str, int* _ret_strsize, char* _filename);
int parse_config_str(work_config* _ret_config, cmd_params _params);

#endif /* HDR_PARSERS_PARSE_CONFIG_H_ */
