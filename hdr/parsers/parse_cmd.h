#ifndef HDR_PARSERS_PARSE_CMD_H_
#define HDR_PARSERS_PARSE_CMD_H_

#include "../libs.h"

typedef enum
{
	CMD_FILE = 0xA0,
	CMD_NET = 0xA1
} read_from;

typedef struct
{
	// file or network adders
	int read_config_type;
	//IP or filename
	char* recive_config_addr;
	//port for network config type
	int recive_config_port;
} cmd_params;

int parse_cmd_params(cmd_params* _retParam, int _argc, char **_argv);

#endif /* HDR_PARSERS_PARSE_CMD_H_ */
