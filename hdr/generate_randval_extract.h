#ifndef HDR_GENERATE_RANDVAL_EXTRACT_H_
#define HDR_GENERATE_RANDVAL_EXTRACT_H_

#include "libs.h"
#include "randnum_gen/randnum.h"

typedef struct
{
	unsigned int gen_quantity;
	unsigned int* gen_type;
	double* gen_prob;
	double* param1;
	double* param2;
	double* offset;
} generator_distr;

int quicksort_double_arr(double** _ret_arr, int _first, int _last);
int add_subarray_2_double_array(double** _target_arr, unsigned int* _target_size, double* _subarr, unsigned int _subarr_size);

int generate_val_interval(double *_ret_val, unsigned int _gen_type, double _par1, double _par2, double _par3);
int generate_val_arr_4_combined_dist(double **_ret_arr, unsigned int* _ret_arr_quantity, double _gen_val, unsigned int _gen_quantity, unsigned int* _gen_type, double* _gen_prob, double* _par1, double* _par2, double* _par3);
int generate_val_arr_4_one_dist(double **_ret_arr, unsigned int* _ret_arr_quantity, double _gen_val, unsigned int _gen_type, double _par1, double _par2, double _par3);
int generate_pack_stream_intervals(double** _ret_arr, unsigned int* _ret_arr_size, double _gen_val, unsigned int _generator_distrs_quantity, generator_distr* _distrs);

#endif /* HDR_GENERATE_RANDVAL_EXTRACT_H_ */
