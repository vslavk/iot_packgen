#ifndef HDR_LIBS_H_
#define HDR_LIBS_H_

#define _USE_MATH_DEFINES
#define TRUE 1
#define FALSE 0
#define NULL (void*)(0)

//Some C libraries
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>
#include <unistd.h>

//Some C libraries for data transmitting
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <signal.h>
#include <netdb.h>

//Libcoap library
#include <coap2/coap.h>

//Parson library
#include "parsers/parson.h"

typedef enum
{
	NO_ERR = 0x00,
	ERR_EXITAPP = 0x01,
	ERR_CMD_UNKOWN_TYPE = 0x02,
	ERR_CMD_UNREC_PARAM = 0x03,
	ERR_CMD_FILE_PARAM = 0x04,
	ERR_CMD_NET_PARAM = 0x05,
	ERR_READFILE = 0x06,
	ERR_PARSE_JSON = 0x07
} error_codes;

#endif
