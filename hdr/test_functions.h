#ifndef HDR_TEST_FUNCTIONS_H_
#define HDR_TEST_FUNCTIONS_H_

#include "libs.h"
#include "generate_randval_extract.h"
#include "randnum_gen/randnum.h"

long test_get_nanos(void);
void test_randnum_generators(void);
void test_interval_generator(void);

#endif /* HDR_TEST_FUNCTIONS_H_ */
