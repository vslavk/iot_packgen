#*IoT traffic generator v.2.0*
---
This software is an updated version of the Internet of Things traffic generation system to simulate the operations of IoT devices.

The following third-party libraries were used to write this software:  
- parson (https://github.com/kgabis/parson);
- libcoap (https://github.com/obgm/libcoap).
---
  
#*Генератор трафика Интернета вещей v.2.0*
---
Данное программное обеспечение представляет собой обновленную версию генератора трафика для имитации работы устройств Интернета вещей.

Для написания данного ПО были использованы следующие сторонние библиотеки:   
- parson (https://github.com/kgabis/parson);
- libcoap (https://github.com/obgm/libcoap).