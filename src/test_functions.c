#include "../hdr/test_functions.h"

long test_get_nanos(void)
{
    struct timespec ts;
    timespec_get(&ts, TIME_UTC);
    return (long)ts.tv_sec * 1000000000L + ts.tv_nsec;
}

void test_randnum_generators(void)
{
	double newnum = 0;

	//double* rand_arr;
	//get_random_str(&rand_arr, 55);

	int count = 0;
	while (count < 1000)
	{
		long time_before = test_get_nanos();
		//gen_weibull_randnum(&newnum, 10, 0.07, 0.000001);
		//gen_exp_randnum(&newnum, 2);
		//gen_norm_randnum (&newnum, 0.5, 0.1);
		//gen_pareto_randnum(&newnum, 10, 1);
		//gen_chi2_randnum (&newnum, 5);
		//gen_arcsin_randnum (&newnum, 5, 3);
		//gen_simson_randnum (&newnum, 2, 8);
		//gen_uniform_randnum (&newnum, 3, 10);
		gen_gamma_randnum (&newnum, 37.6, 10);
		fprintf(stdout, "%lf, %ld\n", newnum, test_get_nanos() - time_before);
		count++;
	}

	//free(rand_arr);
}

void test_interval_generator(void)
{
		int ix;

		/*generator_distr* test_distr = (generator_distr*)malloc(sizeof(generator_distr));
		test_distr[0].gen_quantity = 4;

		test_distr[0].gen_type = (unsigned int*)malloc(4*sizeof(unsigned int));
		test_distr[0].gen_type[0] = RNGEN_WEIBULL;
		test_distr[0].gen_type[1] = RNGEN_WEIBULL;
		test_distr[0].gen_type[2] = RNGEN_ARCSIN;
		test_distr[0].gen_type[3] = RNGEN_UNIFORM;

		test_distr[0].gen_prob = (double*)malloc(4*sizeof(double));
		test_distr[0].gen_prob[0] = 27.6;
		test_distr[0].gen_prob[1] = 51.1;
		test_distr[0].gen_prob[2] = 10.0;
		test_distr[0].gen_prob[3] = 11.3;

		test_distr[0].par1 = (double*)malloc(4*sizeof(double));
		test_distr[0].par1[0] = 10;
		test_distr[0].par1[1] = 21;
		test_distr[0].par1[2] = 0.2;
		test_distr[0].par1[3] = 0.03;

		test_distr[0].par2 = (double*)malloc(4*sizeof(double));
		test_distr[0].par2[0] = 0.07;
		test_distr[0].par2[1] = 0.1;
		test_distr[0].par2[2] = 0.07;
		test_distr[0].par2[3] = 0.4;

		test_distr[0].par3 = (double*)malloc(4*sizeof(double));
		test_distr[0].par3[0] = 0;
		test_distr[0].par3[1] = 0;
		test_distr[0].par3[2] = 0;
		test_distr[0].par3[3] = 0;
		*/

		generator_distr* test_distr = (generator_distr*)malloc(2*sizeof(generator_distr));
		test_distr[0].gen_quantity = 1;
		test_distr[1].gen_quantity = 1;

		test_distr[0].gen_type = (unsigned int*)malloc(sizeof(unsigned int));
		test_distr[0].gen_type[0] = RNGEN_WEIBULL;
		test_distr[1].gen_type = (unsigned int*)malloc(sizeof(unsigned int));
		test_distr[1].gen_type[0] = RNGEN_ARCSIN;

		test_distr[0].gen_prob = (double*)malloc(sizeof(double));
		test_distr[0].gen_prob[0] = 100;
		test_distr[1].gen_prob = (double*)malloc(sizeof(double));
		test_distr[1].gen_prob[0] = 100;

		test_distr[0].param1 = (double*)malloc(sizeof(double));
		test_distr[0].param1[0] = 10;
		test_distr[1].param1 = (double*)malloc(sizeof(double));
		test_distr[1].param1[0] = 5;

		test_distr[0].param2 = (double*)malloc(sizeof(double));
		test_distr[0].param2[0] = 0.07;
		test_distr[1].param2 = (double*)malloc(sizeof(double));
		test_distr[1].param2[0] = 3;

		test_distr[0].offset = (double*)malloc(sizeof(double));
		test_distr[0].offset[0] = 0.000001;
		test_distr[1].offset = (double*)malloc(sizeof(double));
		test_distr[1].offset[0] = 0;

		double* time_intervals;
		unsigned int time_intervals_quantity = 0;
		generate_pack_stream_intervals(&time_intervals, &time_intervals_quantity, 500, 2, test_distr);

		for(ix = 1; ix <= time_intervals_quantity; ix++)
		{
			fprintf(stdout, "%lf", time_intervals[ix-1]);
			if((ix%5) != 0)
			{
				fprintf(stdout, ", ");
			}
			else
			{
				fprintf(stdout, "\n");
			}
		}

		free(time_intervals);

		for(ix = 0; ix < 1; ix++)
		{
			free(test_distr[ix].gen_type);
			free(test_distr[ix].gen_prob);
			free(test_distr[ix].param1);
			free(test_distr[ix].param2);
			free(test_distr[ix].offset);
		}

		free(test_distr);
}

void test_read_file_and_create_config_struct(int _argc, char **_argv)
{
	cmd_params settings;
	parse_cmd_params(&settings, _argc, _argv);

	fprintf(stdout, "cmd par is %x, %s, %d\n", settings.read_config_type, settings.recive_config_addr, settings.recive_config_port);

	work_config work_con;
	parse_config_str(&work_con, settings);

	fprintf(stdout, "work par is %s, %s, %d, %d\n", work_con.dst_addr, work_con.dst_mask, work_con.dst_port, work_con.stream_conf_quantity);
	int ix;
	for (ix = 0; ix < work_con.stream_conf_quantity; ix++)
	{
		fprintf(stdout, "\tstream par is %d, %lf, %d, %d, %d\n", work_con.streams_configs[ix].protocol, work_con.streams_configs[ix].gen_time, work_con.streams_configs[ix].attached_streams, work_con.streams_configs[ix].time_distrs_quantity, work_con.streams_configs[ix].packsize_distrs_quantity);
		int iy;
		for(iy = 0; iy < work_con.streams_configs[ix].time_distrs_quantity; iy++)
		{
			fprintf(stdout, "\t\ttime distr par is %x, %lf, %lf, %lf, %lf\n", work_con.streams_configs[ix].time_distrs[iy].distr_type, work_con.streams_configs[ix].time_distrs[iy].hit_prob, work_con.streams_configs[ix].time_distrs[iy].param1, work_con.streams_configs[ix].time_distrs[iy].param2, work_con.streams_configs[ix].time_distrs[iy].offset);
		}
		for(iy = 0; iy < work_con.streams_configs[ix].packsize_distrs_quantity; iy++)
		{
			fprintf(stdout, "\t\tpacksize distr par is %x, %lf, %lf, %lf, %lf\n", work_con.streams_configs[ix].packsize_distrs[iy].distr_type, work_con.streams_configs[ix].packsize_distrs[iy].hit_prob, work_con.streams_configs[ix].packsize_distrs[iy].param1, work_con.streams_configs[ix].packsize_distrs[iy].param2, work_con.streams_configs[ix].packsize_distrs[iy].offset);
		}
	}
}
