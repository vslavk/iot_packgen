#include "../../hdr/parsers/parse_config.h"

int read_config_file(char** _ret_str, int* _ret_strsize, char* _filename)
{
	int ret_status = NO_ERR;

	FILE* config_file = fopen(_filename, "r");
	if (config_file == NULL){
	    return ERR_READFILE;
	}
	fseek(config_file, 0, SEEK_END);
	(*_ret_strsize) = ftell(config_file) + 1;

	(*_ret_str) = (char*)malloc((*_ret_strsize) * sizeof(char));
	char buf_char = 0x00;
	int buf_str_count = 0;

	fseek(config_file, 0, SEEK_SET);
	while(buf_char != EOF)
	{
		buf_char = fgetc(config_file);
		(*_ret_str)[buf_str_count] = buf_char;
		buf_str_count++;
	}

	fclose(config_file);

	return ret_status;
}

int parse_config_str(work_config* _ret_config, cmd_params _params)
{
	int ret_status = NO_ERR;

	char *buf_str;
	int buf_size;
	if(_params.read_config_type == CMD_FILE)
	{
		read_config_file(&buf_str, &buf_size, _params.recive_config_addr);
		char *st_comment = strstr(buf_str, "/*");
		char *en_comment = strstr(buf_str, "*/");

		while(st_comment != NULL || en_comment != NULL)
		{
			int buf_pos_diff = en_comment - st_comment + 2;

			memmove(st_comment, en_comment+2, (buf_str + buf_size) - (en_comment + 2) - 1);
			buf_size = buf_size - buf_pos_diff;
			buf_str = (char*)realloc(buf_str, buf_size*sizeof(char));

			st_comment = strstr(buf_str, "/*");
			en_comment = strstr(buf_str, "*/");
		}
	}
	else if(_params.read_config_type == CMD_NET)
	{

	}

	if (_params.read_config_type == CMD_FILE ||_params.read_config_type == CMD_NET)
	{
		/* parsing json and validating output */
		JSON_Value *root_value = json_parse_string(buf_str);
		if (json_value_get_type(root_value) != JSONObject)
		{
			return ERR_PARSE_JSON;
		}

		/* getting array from root value and printing commit info */
		int ix;
		JSON_Object *prog_config = json_value_get_object(root_value);
		(*_ret_config).dst_addr = (char*)json_object_get_string(prog_config, "dst_addr");
		(*_ret_config).dst_mask = (char*)json_object_get_string(prog_config, "dst_mask");
		(*_ret_config).dst_port = json_object_get_number(prog_config, "dst_port");
		(*_ret_config).stream_conf_quantity = json_object_get_number(prog_config, "stream_conf_quantity");

		stream_config *buf_st = (stream_config*)malloc((*_ret_config).stream_conf_quantity * sizeof(stream_config));
		JSON_Array *streams_arr = json_object_get_array(prog_config, "streams_configs");
		for (ix = 0; ix < json_array_get_count(streams_arr); ix++)
		{
			JSON_Object *buf_obj = json_array_get_object(streams_arr, ix);
			char *buf_protocol = (char*)json_object_get_string(buf_obj, "protocol");
			if(!strcmp(buf_protocol, "udp"))
			{
				buf_st[ix].protocol = GENPROT_UDP;
			}
			else if(!strcmp(buf_protocol, "tcp"))
			{
				buf_st[ix].protocol = GENPROT_TCP;
			}
			else if(!strcmp(buf_protocol, "httpv1"))
			{
				buf_st[ix].protocol = GENPROT_HTTPv1;
				buf_st[ix].uri = (char*)json_object_get_string(buf_obj, "uri");
				buf_st[ix].method = (char*)json_object_get_string(buf_obj, "method");
				buf_st[ix].data_type = (char*)json_object_get_string(buf_obj, "data_type");
			}
			else if(!strcmp(buf_protocol, "httpv2"))
			{
				buf_st[ix].protocol = GENPROT_HTTPv2;
				buf_st[ix].uri = (char*)json_object_get_string(buf_obj, "uri");
				buf_st[ix].method = (char*)json_object_get_string(buf_obj, "method");
				buf_st[ix].data_type = (char*)json_object_get_string(buf_obj, "data_type");
			}
			else if(!strcmp(buf_protocol, "websocket"))
			{
				buf_st[ix].protocol = GENPROT_WEBSOCKET;
			}
			else if(!strcmp(buf_protocol, "coap"))
			{
				buf_st[ix].protocol = GENPROT_COAP;
			}
			else if(!strcmp(buf_protocol, "mqtt"))
			{
				buf_st[ix].protocol = GENPROT_MQTT;
				buf_st[ix].topic = (char*)json_object_get_string(buf_obj, "topic");
			}
			else if(!strcmp(buf_protocol, "stomp"))
			{
				buf_st[ix].protocol = GENPROT_STOMP;
			}
			else if(!strcmp(buf_protocol, "opcua"))
			{
				buf_st[ix].protocol = GENPROT_OPCUA;
			}
			else if(!strcmp(buf_protocol, "modbus"))
			{
				buf_st[ix].protocol = GENPROT_MODBUS;
			}

			buf_st[ix].gen_time = json_object_get_number(buf_obj, "generation_time");
			buf_st[ix].attached_streams = json_object_get_number(buf_obj, "attached_streams");

			buf_st[ix].time_distrs_quantity = json_object_get_number(buf_obj, "time_dist_quantity");
			buf_st[ix].time_distrs = (distr_config*)malloc(buf_st[ix].time_distrs_quantity * sizeof(distr_config));
			JSON_Array *time_distr_arr = json_object_get_array(buf_obj, "time_distr");
			int iy;

			for (iy = 0; iy < json_array_get_count(time_distr_arr); iy++)
			{
				JSON_Object *buf_obj2 = json_array_get_object(time_distr_arr, iy);
				buf_st[ix].time_distrs[iy].hit_prob = json_object_get_number(buf_obj2, "hit_probabity");

				char *buf_distr_type = (char*)json_object_get_string(buf_obj2, "distr_type");
				if(!strcmp(buf_distr_type, "uniform") || !strcmp(buf_distr_type, "UNIFORM"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_UNIFORM;
				}
				else if(!strcmp(buf_distr_type, "exp") || !strcmp(buf_distr_type, "EXP") ||
						!strcmp(buf_distr_type, "exponential") || !strcmp(buf_distr_type, "EXPONENTIAL"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_EXP;
				}
				else if(!strcmp(buf_distr_type, "norm") || !strcmp(buf_distr_type, "NORM") ||
						!strcmp(buf_distr_type, "normal") || !strcmp(buf_distr_type, "NORMAL") ||
						!strcmp(buf_distr_type, "gauss") || !strcmp(buf_distr_type, "GAUSS"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_NORM;
				}
				else if(!strcmp(buf_distr_type, "weibull") || !strcmp(buf_distr_type, "WEIBULL"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_WEIBULL;
				}
				else if(!strcmp(buf_distr_type, "pareto") || !strcmp(buf_distr_type, "PARETO"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_PARETO;
				}
				else if(!strcmp(buf_distr_type, "chi2") || !strcmp(buf_distr_type, "CHI2"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_CHI2;
				}
				else if(!strcmp(buf_distr_type, "arcsin") || !strcmp(buf_distr_type, "ARCSIN"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_ARCSIN;
				}
				else if(!strcmp(buf_distr_type, "simson") || !strcmp(buf_distr_type, "SIMSON"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_SIMSON;
				}
				else if(!strcmp(buf_distr_type, "gamma") || !strcmp(buf_distr_type, "GAMMA"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_GAMMA;
				}
				else if(!strcmp(buf_distr_type, "erlang") || !strcmp(buf_distr_type, "ERLANG") ||
						!strcmp(buf_distr_type, "erl") || !strcmp(buf_distr_type, "ERL"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_ERLANG;
				}
				else if(!strcmp(buf_distr_type, "const") || !strcmp(buf_distr_type, "CONST"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_CONST;
				}
				buf_st[ix].time_distrs[iy].param1 = json_object_get_number(buf_obj2, "param1");
				buf_st[ix].time_distrs[iy].param2 = json_object_get_number(buf_obj2, "param2");
				buf_st[ix].time_distrs[iy].offset = json_object_get_number(buf_obj2, "offset");
			}

			buf_st[ix].packsize_distrs_quantity = json_object_get_number(buf_obj, "packsize_distr_quantity");
			buf_st[ix].packsize_distrs = (distr_config*)malloc(buf_st[ix].packsize_distrs_quantity * sizeof(distr_config));
			JSON_Array *packsize_distr_arr = json_object_get_array(buf_obj, "packsize_distr");

			for (iy = 0; iy < json_array_get_count(packsize_distr_arr); iy++)
			{
				JSON_Object *buf_obj2 = json_array_get_object(packsize_distr_arr, iy);
				buf_st[ix].packsize_distrs[iy].hit_prob = json_object_get_number(buf_obj2, "hit_probabity");

				char *buf_distr_type = (char*)json_object_get_string(buf_obj2, "distr_type");
				if(!strcmp(buf_distr_type, "uniform") || !strcmp(buf_distr_type, "UNIFORM"))
				{
					buf_st[ix].packsize_distrs[iy].distr_type = RNGEN_UNIFORM;
				}
				else if(!strcmp(buf_distr_type, "exp") || !strcmp(buf_distr_type, "EXP") ||
						!strcmp(buf_distr_type, "exponential") || !strcmp(buf_distr_type, "EXPONENTIAL"))
				{
					buf_st[ix].packsize_distrs[iy].distr_type = RNGEN_EXP;
				}
				else if(!strcmp(buf_distr_type, "norm") || !strcmp(buf_distr_type, "NORM") ||
						!strcmp(buf_distr_type, "normal") || !strcmp(buf_distr_type, "NORMAL") ||
						!strcmp(buf_distr_type, "gauss") || !strcmp(buf_distr_type, "GAUSS"))
				{
					buf_st[ix].packsize_distrs[iy].distr_type = RNGEN_NORM;
				}
				else if(!strcmp(buf_distr_type, "weibull") || !strcmp(buf_distr_type, "WEIBULL"))
				{
					buf_st[ix].packsize_distrs[iy].distr_type = RNGEN_WEIBULL;
				}
				else if(!strcmp(buf_distr_type, "pareto") || !strcmp(buf_distr_type, "PARETO"))
				{
					buf_st[ix].packsize_distrs[iy].distr_type = RNGEN_PARETO;
				}
				else if(!strcmp(buf_distr_type, "chi2") || !strcmp(buf_distr_type, "CHI2"))
				{
					buf_st[ix].packsize_distrs[iy].distr_type = RNGEN_CHI2;
				}
				else if(!strcmp(buf_distr_type, "arcsin") || !strcmp(buf_distr_type, "ARCSIN"))
				{
					buf_st[ix].packsize_distrs[iy].distr_type = RNGEN_ARCSIN;
				}
				else if(!strcmp(buf_distr_type, "simson") || !strcmp(buf_distr_type, "SIMSON"))
				{
					buf_st[ix].packsize_distrs[iy].distr_type = RNGEN_SIMSON;
				}
				else if(!strcmp(buf_distr_type, "gamma") || !strcmp(buf_distr_type, "GAMMA"))
				{
					buf_st[ix].packsize_distrs[iy].distr_type = RNGEN_GAMMA;
				}
				else if(!strcmp(buf_distr_type, "erlang") || !strcmp(buf_distr_type, "ERLANG") ||
						!strcmp(buf_distr_type, "erl") || !strcmp(buf_distr_type, "ERL"))
				{
					buf_st[ix].time_distrs[iy].distr_type = RNGEN_ERLANG;
				}
				else if(!strcmp(buf_distr_type, "const") || !strcmp(buf_distr_type, "CONST"))
				{
					buf_st[ix].packsize_distrs[iy].distr_type = RNGEN_CONST;
				}
				buf_st[ix].packsize_distrs[iy].param1 = json_object_get_number(buf_obj2, "param1");
				buf_st[ix].packsize_distrs[iy].param2 = json_object_get_number(buf_obj2, "param2");
				buf_st[ix].packsize_distrs[iy].offset = json_object_get_number(buf_obj2, "offset");
			}
		}
		(*_ret_config).streams_configs = buf_st;
	}

	free(buf_str);

	return ret_status;
}
