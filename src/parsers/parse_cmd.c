#include "../../hdr/parsers/parse_cmd.h"

int parse_cmd_params(cmd_params* _retParam, int _argc, char **_argv)
{
	int retStatus = NO_ERR;

	int ix = 1;
	while (ix < _argc)
	{
		if( !strcmp((const char*)_argv[ix], "-t") || !strcmp((const char*)_argv[ix], "type") )
		{
			if( !strcmp((const char*)_argv[ix+1], "file") )
			{
				(*_retParam).read_config_type = CMD_FILE;
				(*_retParam).recive_config_port = 0;
				ix = ix + 2;
			}
			else if ( !strcmp((const char*)_argv[ix+1], "net") )
			{
				(*_retParam).read_config_type = CMD_NET;
				ix = ix + 2;
			}
			else
			{
				retStatus = ERR_CMD_UNKOWN_TYPE;
				break;
			}
		}
		else if ( !strcmp((const char*)_argv[ix], "-f") || !strcmp((const char*)_argv[ix], "filename") )
		{
			if((*_retParam).read_config_type == CMD_FILE)
			{
				(*_retParam).recive_config_addr = (char*)malloc((strlen(_argv[ix+1]) + 1) * sizeof(char));
				(*_retParam).recive_config_addr = _argv[ix+1];
				ix = ix + 2;
			}
			else
			{
				(*_retParam).recive_config_addr = "NULL";
				retStatus = ERR_CMD_UNREC_PARAM;
				break;
			}
		}
		else if ( !strcmp((const char*)_argv[ix], "-h") || !strcmp((const char*)_argv[ix], "host") )
		{
			if((*_retParam).read_config_type == CMD_NET)
			{
				(*_retParam).recive_config_addr = (char*)malloc((strlen(_argv[ix+1]) + 1) * sizeof(char));
				(*_retParam).recive_config_addr = _argv[ix+1];
				ix = ix + 2;
			}
			else
			{
				(*_retParam).recive_config_addr = "NULL";
				retStatus = ERR_CMD_UNREC_PARAM;
				break;
			}
		}
		else if ( !strcmp((const char*)_argv[ix], "-p") || !strcmp((const char*)_argv[ix], "port") )
		{
			if((*_retParam).read_config_type == CMD_NET)
			{
				(*_retParam).recive_config_port = atoi(_argv[ix+1]);
				ix = ix + 2;
			}
			else
			{
				(*_retParam).recive_config_port = 0;
				retStatus = ERR_CMD_UNREC_PARAM;
				break;
			}
		}
	}
	if( (*_retParam).read_config_type == CMD_FILE  && !strcmp((*_retParam).recive_config_addr, "NULL") )
	{
		retStatus = ERR_CMD_FILE_PARAM;
	}
	else if( (*_retParam).read_config_type == CMD_NET  && (!strcmp((*_retParam).recive_config_addr, "NULL") || ((*_retParam).recive_config_port == 0)))
	{
		retStatus = ERR_CMD_NET_PARAM;
	}

	return retStatus;
}
