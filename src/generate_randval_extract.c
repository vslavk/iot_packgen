#include "../hdr/generate_randval_extract.h"

int quicksort_double_arr(double** _ret_arr, int _first, int _last)
{
	int _ret_status = NO_ERR;
	double mid, count;
	int buf_f = _first, buf_l =_last;
	mid = (*_ret_arr)[(buf_f + buf_l) / 2];
	do
	{
		while ((*_ret_arr)[buf_f] < mid)
		{
			buf_f++;
		}
		while ((*_ret_arr)[buf_l] > mid)
		{
			buf_l--;
		}
		if (buf_f <= buf_l)
		{
			count = (*_ret_arr)[buf_f];
			(*_ret_arr)[buf_f] = (*_ret_arr)[buf_l];
			(*_ret_arr)[buf_l] = count;
			buf_f++;
			buf_l--;
		}
	} while (buf_f < buf_l);

	if (_first < buf_l)
	{
		quicksort_double_arr(_ret_arr, _first, buf_l);
	}

	if (buf_f < _last)
	{
		quicksort_double_arr(_ret_arr, buf_f, _last);
	}

    return _ret_status;
}

int generate_val_interval(double *_ret_val, unsigned int _gen_type, double _par1, double _par2, double _par3)
{
	int ret_status = NO_ERR;

	double buf_num = 0;
	switch(_gen_type)
	{
		case(RNGEN_UNIFORM):
			gen_uniform_randnum (&buf_num, _par1, _par2);
			(*_ret_val) = buf_num;
			break;
		case(RNGEN_EXP):
			gen_exp_randnum (&buf_num, _par1);
			(*_ret_val) = buf_num;
			break;
		case(RNGEN_NORM):
			gen_norm_randnum (&buf_num, _par1, _par2);
			(*_ret_val) = buf_num;
			break;
		case(RNGEN_WEIBULL):
			gen_weibull_randnum (&buf_num, _par1, _par2, _par3);
			(*_ret_val) = buf_num;
			break;
		case(RNGEN_PARETO):
			gen_pareto_randnum (&buf_num, _par1, _par2);
			(*_ret_val) = buf_num;
			break;
		case(RNGEN_CHI2):
			gen_chi2_randnum (&buf_num, _par1);
			(*_ret_val) = buf_num;
			break;
		case(RNGEN_ARCSIN):
			gen_arcsin_randnum (&buf_num, _par1, _par2);
			(*_ret_val) = buf_num;
			break;
		case(RNGEN_SIMSON):
			gen_simson_randnum (&buf_num, _par1, _par2);
			(*_ret_val) = buf_num;
			break;
		default:
			(*_ret_val) = _par1;
			break;
	}

	return ret_status;
}

int generate_val_arr_4_combined_dist(double **_ret_arr, unsigned int* _ret_arr_quantity, double _gen_val, unsigned int _gen_quantity, unsigned int* _gen_type, double* _gen_prob, double* _par1, double* _par2, double* _par3)
{
	int ret_status = NO_ERR;

	double current_time = 0;

	(*_ret_arr) = (double*)malloc(sizeof(double));

	unsigned int count = 1;
	double buf_interval = 0;
	while(current_time <= _gen_val)
	{
		double buf_rand_val = ((double)rand() / RAND_MAX)*100;
		double buf_prob = 0;
		int ix;
		for (ix = 0; ix < _gen_quantity; ++ix)
		{
			buf_prob = buf_prob + _gen_prob[ix];
			if (buf_prob <= buf_rand_val || ix == (_gen_quantity - 1))
			{
				generate_val_interval(&buf_interval, _gen_type[ix], _par1[ix], _par2[ix], _par3[ix]);
				current_time = current_time + buf_interval;
				(*_ret_arr)[count-1] = current_time;
				count++;
				(*_ret_arr) = (double*)realloc((*_ret_arr), count*sizeof(double));
			}
		}
	}

	(*_ret_arr_quantity) = count;

	return ret_status;
}

int generate_val_arr_4_one_dist(double **_ret_arr, unsigned int* _ret_arr_quantity, double _gen_val, unsigned int _gen_type, double _par1, double _par2, double _par3)
{
	int ret_status = NO_ERR;

	(*_ret_arr) = (double*)malloc(sizeof(double));

	unsigned int count = 1;
	double current_time = 0;
	double buf_num = 0;
	switch(_gen_type)
	{
		case(RNGEN_UNIFORM):
			while(current_time <= _gen_val)
			{
				gen_uniform_randnum (&buf_num, _par1, _par2);
				current_time = current_time + buf_num;
				(*_ret_arr)[count-1] = current_time;
				count++;
				(*_ret_arr) = (double*)realloc((*_ret_arr), count*sizeof(double));
			}
			break;
		case(RNGEN_EXP):
			while(current_time <= _gen_val)
			{
				gen_exp_randnum (&buf_num, _par1);
				current_time = current_time + buf_num;
				(*_ret_arr)[count-1] = current_time;
				count++;
				(*_ret_arr) = (double*)realloc((*_ret_arr), count*sizeof(double));
			}
			break;
		case(RNGEN_NORM):
			while(current_time <= _gen_val)
			{
				gen_norm_randnum (&buf_num, _par1, _par2);
				current_time = current_time + buf_num;
				(*_ret_arr)[count-1] = current_time;
				count++;
				(*_ret_arr) = (double*)realloc((*_ret_arr), count*sizeof(double));
			}
			break;
		case(RNGEN_WEIBULL):
			while(current_time <= _gen_val)
			{
				gen_weibull_randnum (&buf_num, _par1, _par2, _par3);
				current_time = current_time + buf_num;
				(*_ret_arr)[count-1] = current_time;
				count++;
				(*_ret_arr) = (double*)realloc((*_ret_arr), count*sizeof(double));
			}
			break;
		case(RNGEN_PARETO):
			while(current_time <= _gen_val)
			{
				gen_pareto_randnum (&buf_num, _par1, _par2);
				current_time = current_time + buf_num;
				(*_ret_arr)[count-1] = current_time;
				count++;
				(*_ret_arr) = (double*)realloc((*_ret_arr), count*sizeof(double));
			}
			break;
		case(RNGEN_CHI2):
			while(current_time <= _gen_val)
			{
				gen_chi2_randnum (&buf_num, _par1);
				current_time = current_time + buf_num;
				(*_ret_arr)[count-1] = current_time;
				count++;
				(*_ret_arr) = (double*)realloc((*_ret_arr), count*sizeof(double));
			}
			break;
		case(RNGEN_ARCSIN):
			while(current_time <= _gen_val)
			{
				gen_arcsin_randnum (&buf_num, _par1, _par2);
				current_time = current_time + buf_num;
				(*_ret_arr)[count-1] = current_time;
				count++;
				(*_ret_arr) = (double*)realloc((*_ret_arr), count*sizeof(double));
			}
			break;
		case(RNGEN_SIMSON):
			while(current_time <= _gen_val)
			{
				gen_simson_randnum (&buf_num, _par1, _par2);
				current_time = current_time + buf_num;
				(*_ret_arr)[count-1] = current_time;
				count++;
				(*_ret_arr) = (double*)realloc((*_ret_arr), count*sizeof(double));
			}
			break;
		default:
			while(current_time <= _gen_val)
			{
				current_time = current_time + _par1;
				(*_ret_arr)[count-1] = current_time;
				count++;
				(*_ret_arr) = (double*)realloc((*_ret_arr), count*sizeof(double));
			}
			break;
	}

	(*_ret_arr_quantity) = count;

	return ret_status;
}

int add_subarray_2_double_array(double** _target_arr, unsigned int* _target_size, double* _subarr, unsigned int _subarr_size)
{
	int ret_status = NO_ERR;

	unsigned int new_size = (*_target_size) + _subarr_size;
	(*_target_arr) = (double*)realloc((*_target_arr), new_size*sizeof(double));

	int ix;
	for(ix = (*_target_size); ix < new_size; ix++)
	{
		(*_target_arr)[ix] = _subarr[ix];
	}
	(*_target_size) = new_size;

	return ret_status;
}

int generate_pack_stream_intervals(double** _ret_arr, unsigned int* _ret_arr_size, double _gen_time, unsigned int _generator_distrs_quantity, generator_distr* _distrs)
{
	int ret_status = NO_ERR;

	double* time_arr = (double*)malloc(sizeof(double));
	time_arr[0] = 0;
	unsigned int time_arr_size = 1;
	int ix;
	for (ix = 0; ix < _generator_distrs_quantity; ix++)
	{
		unsigned int buf_gen_quan = _distrs[ix].gen_quantity;

		if(buf_gen_quan > 1)
		{
			double* buf_arr;
			unsigned int buf_arr_quantity;
			generate_val_arr_4_combined_dist(&buf_arr, &buf_arr_quantity, _gen_time, buf_gen_quan, _distrs[ix].gen_type, _distrs[ix].gen_prob, _distrs[ix].param1, _distrs[ix].param2, _distrs[ix].offset);
			add_subarray_2_double_array(&time_arr, &time_arr_size, buf_arr, buf_arr_quantity);
			free(buf_arr);
		}
		else if (buf_gen_quan == 1)
		{
			double* buf_arr;
			unsigned int buf_arr_quantity;
			generate_val_arr_4_one_dist(&buf_arr, &buf_arr_quantity, _gen_time, _distrs[ix].gen_type[0], _distrs[ix].param1[0], _distrs[ix].param2[0], _distrs[ix].offset[0]);
			add_subarray_2_double_array(&time_arr, &time_arr_size, buf_arr, buf_arr_quantity);
			free(buf_arr);
		}
	}

	(*_ret_arr) = (double*)malloc((time_arr_size-1)*sizeof(double));
	quicksort_double_arr(&time_arr, 0, (time_arr_size-1));

	int interval_count = 0;
	for (ix = 1; ix < time_arr_size; ix++)
	{
		double buf_interval = time_arr[ix] - time_arr[ix-1];
		if(buf_interval > 0.000001)
		{
			(*_ret_arr)[interval_count] = buf_interval;
			interval_count++;
		}
	}
	(*_ret_arr) = (double*)realloc((*_ret_arr), interval_count*sizeof(double));


	(*_ret_arr_size) = interval_count;

	free(time_arr);

	return ret_status;
}

