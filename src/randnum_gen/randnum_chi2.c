#include "../../hdr/randnum_gen/randnum_weibull.h"

int gen_chi2_randnum (double* _ret_num, double _n_par)
{
	int ret_status = NO_ERR;

	double buf_normnum = 0;
	int ix = 0;
	for(ix = 0; ix < _n_par; ix++)
	{
		double buf_num = 0;
		int iy = 0;
		for (iy = 0; iy < 3; iy++)
		{
			buf_num = buf_num + (double)rand() / RAND_MAX;
		}
		buf_num = 2 * ( buf_num - 1.5 );
		buf_normnum = buf_normnum + pow(buf_num, 2);
	}
	*_ret_num = buf_normnum;

	return ret_status;
}
