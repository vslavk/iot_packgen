#include "../../hdr/randnum_gen/randnum_exp.h"

int gen_exp_randnum (double* _ret_num, double _l_par)
{
	int ret_status = NO_ERR;

	double buf_un_num = (double)rand() / RAND_MAX;
	*_ret_num = -log(buf_un_num)/_l_par;

	return ret_status;
}
