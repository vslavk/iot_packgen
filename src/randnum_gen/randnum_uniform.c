#include "../../hdr/randnum_gen/randnum_uniform.h"

int get_random_str (double** _ret_arr, int _str_size)
{
	int ret_status = NO_ERR;

	FILE *pseudofile = fopen("/dev/random", "r");
	if (pseudofile != NULL)
	{
		ret_status = 1;
		*_ret_arr = (double*)malloc((_str_size+1)*sizeof(double));

		char buf_str[_str_size+1];
		if( fgets(buf_str, _str_size, pseudofile) )
		{
			int symbol_count = 0;
			while(symbol_count < _str_size)
			{
				(*_ret_arr)[symbol_count] = fabs((float)buf_str[symbol_count] / 255);
				//fprintf(stdout, "%lf ", (*_ret_arr)[symbol_count]);
				++symbol_count;
			}
		}
		else
		{
			ret_status = 2;
		}
	}
	else
	{
		ret_status = 1;
	}

	//fprintf(stdout, "\n\n");
	fclose(pseudofile);

	return ret_status;
}


int gen_fib_randnum (double* _ret_num, double** _fib_arr, int _arr_size, int _a_par, int _b_par)
{
	int ret_status = 0;


	if (_a_par <= _b_par)
	{
		ret_status = 1;
	}
	else
	{
		if (_a_par > _arr_size)
		{
			ret_status = 2;
		}
		else
		{
			*_fib_arr = (double*)realloc((*_fib_arr), (_arr_size+1)*sizeof(double));

			//double buf_xa = 0, buf_xb = 0;
			//buf_xa = (*_fib_arr)[_arr_size - _a_par];
			//buf_xb = (*_fib_arr)[_arr_size - _b_par];
			//(*_fib_arr)[_arr_size] = (buf_xa >= buf_xb) ? (buf_xa - buf_xb) : (buf_xa - buf_xb + 1);
			(*_fib_arr)[_arr_size] = ((*_fib_arr)[_arr_size - _a_par] >= (*_fib_arr)[_arr_size - _b_par]) ? ((*_fib_arr)[_arr_size - _a_par] - (*_fib_arr)[_arr_size - _b_par]) : ((*_fib_arr)[_arr_size - _a_par] - (*_fib_arr)[_arr_size - _b_par] + 1);

			*_ret_num = (*_fib_arr)[_arr_size];
		}
	}

	return ret_status;
}

int gen_uniform_randnum (double* _ret_num, double _a_par, double _b_par)
{
	int ret_status = 0;

	double buf_un_num = (double)rand() / RAND_MAX;
	*_ret_num = _a_par + (_b_par - _a_par) * buf_un_num;

	return ret_status;
}
