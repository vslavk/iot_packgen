#include "../../hdr/randnum_gen/randnum_pareto.h"

int gen_pareto_randnum (double* _ret_num, double _a_par, double _num_offset)
{
	int ret_status = NO_ERR;

	double buf_un_num = (double)rand() / RAND_MAX;
	*_ret_num = _num_offset * powl(buf_un_num, -(1/_a_par));

	return ret_status;
}
