#include "../../hdr/randnum_gen/randnum_arcsin.h"

int gen_arcsin_randnum (double* _ret_num, double _mu_par, double _l_par)
{
	int ret_status = NO_ERR;

	double buf_un_num = (double)rand() / RAND_MAX;
	*_ret_num = _mu_par + _l_par * sin(M_PI*(buf_un_num - 0.5));

	return ret_status;
}
