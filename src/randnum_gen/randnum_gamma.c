#include "../../hdr/randnum_gen/randnum_gamma.h"

int gen_gamma_method_less_then_one (double* _ret_num, double _alpha, double _lambda)
{
	int ret_status = NO_ERR;

	double buf_v,
		   buf_b = M_E / (M_E + _alpha);
	while(TRUE)
	{
		double buf_rand1 = (double)rand() / RAND_MAX,
			   buf_rand2 = (double)rand() / RAND_MAX;
		if(buf_rand1 < buf_b)
		{
			buf_v = pow(buf_rand1 / buf_b, 1/_alpha);
			if(buf_rand2 <= pow(M_E, -buf_v))
			{
				break;
			}
		}
		else
		{
			buf_v = 1 - log((1 - buf_rand1)/(1 - buf_b));
			if(buf_rand2 <= pow(buf_v, _alpha - 1))
			{
				break;
			}
		}
	}
	(*_ret_num) = buf_v / _lambda;

	return ret_status;
}

int gen_gamma_randnum (double* _ret_num, double _alpha, double _lambda)
{
	int ret_status = NO_ERR;
	double alpha_int = 0,
		   alpha_frac = 0;

	alpha_frac = modf(_alpha, &alpha_int);
	if(_alpha == 1)
	{
		gen_exp_randnum (_ret_num, _lambda);
	}
	else if(alpha_frac == 0)
	{
		gen_erlang_randnum (_ret_num, _alpha, _lambda);
	}
	else if(_alpha < 1)
	{
		gen_gamma_method_less_then_one(_ret_num, _alpha, _lambda);
	}
	else
	{
		double buf_frac_num, buf_int_num;
		gen_gamma_method_less_then_one(&buf_frac_num, alpha_frac, _lambda);
		gen_erlang_randnum (&buf_int_num, alpha_int, _lambda);
		(*_ret_num) = buf_frac_num + buf_int_num;
	}

	return ret_status;
}
