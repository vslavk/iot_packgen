#include "../../hdr/randnum_gen/randnum_erlang.h"

int gen_erlang_randnum (double *_ret_num, double _alpha, double _lambda)
{
	int ret_status = NO_ERR;

	long double buf_comp = 1;
	int im;
	for(im = 0; im < _alpha; im++)
	{
		double buf_un_num = (double)rand() / RAND_MAX;
		buf_comp = buf_comp * buf_un_num;
	}
	(*_ret_num) = (-1/_lambda) * log(buf_comp);

	return ret_status;
}

