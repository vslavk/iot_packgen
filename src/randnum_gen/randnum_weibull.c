#include "../../hdr/randnum_gen/randnum_weibull.h"

int gen_weibull_randnum (double* _ret_num, double _a_par, double _b_par, double _num_offset)
{
	int ret_status = NO_ERR;

	double buf_un_num = (double)rand() / RAND_MAX;
	*_ret_num = _num_offset + _b_par * powl(-log(buf_un_num), (1/_a_par));

	return ret_status;
}
