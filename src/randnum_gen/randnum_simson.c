#include "../../hdr/randnum_gen/randnum_simson.h"

int gen_simson_randnum (double* _ret_num, double _a_par, double _b_par)
{
	int ret_status = NO_ERR;

	double buf_un_num1 = (double)rand() / RAND_MAX;
	double buf_un_num2 = (double)rand() / RAND_MAX;
	*_ret_num = _a_par + ((_b_par - _a_par)/2)*(buf_un_num1 + buf_un_num2);

	return ret_status;
}
