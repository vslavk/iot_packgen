
#include "../../hdr/randnum_gen/randnum_norm.h"

int gen_norm_randnum (double* _ret_num, double _mu_par, double _sig_par)
{
	int ret_status = NO_ERR;

	int ix = 0;
	double buf_un_num = 0;
	for (ix = 0; ix < 3; ix++)
	{
		buf_un_num = buf_un_num + (double)rand() / RAND_MAX;
	}
	buf_un_num = 2 * ( buf_un_num - 1.5 );

	*_ret_num = _mu_par + _sig_par * buf_un_num;

	return ret_status;
}
